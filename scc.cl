;;;; -*- mode: common-lisp -*-
(declaim (optimize safety))

; (require :asdf)
; (require :rt)
; (require :bluewolf-utilities)

(defpackage "SCC"
  (:use "CL")
  (:use "RT")
  (:use "DICE")
  (:export "NEW-GAME"
	   "KEEP-DIE"
	   "PLAY"))
(in-package "SCC")


;; ;; Make fresh random state
;; (setf *random-state* (make-random-state t))


;; Tests:
(deftest 30-d6-rolls-between-1-and-6
    (find-if-not #'(lambda (x) (<= 1 x 6))
		 (loop for i from 1 to 30 collect (dice:d6)))
  nil)


;;; Program state
(defparameter *dice-count* 5
  "At the start of each game there are so many dice.")
(defparameter *dice* ()
  "Hold the dice rolls.")
(defparameter *tries* 3
  "Count the player's throws of dice.")
(defparameter *score* 0
  "Keep the player's score.")
(defparameter *kept-die* nil
  "Hold the die for later scoring.")

(defparameter *ship-p* nil
  "Whether the player has a ship.")
(defparameter *captain-p* nil
  "Whether the player has a captain.")
(defparameter *crew-p* nil
  "Whether the player has a crew.")

;;; Logic
(defconstant +ship+ 6)
(defconstant +captain+ 5)
(defconstant +crew+ 4)

(defun roll ()
  "Make a list of *DICE-COUNT* dice rolls."
  (loop for i from 1 to *dice-count* collect (dice:d6)))

;; Simple membership tests
(defun have-ship-p (dice)
  "Determine whether DICE contains a 6, denoting a ship."
  (member +ship+ dice))

(defun have-captain-p (dice)
  "Determine whether DICE contains a 5, denoting a captain."
  (member +captain+ dice))

(defun have-crew-p (dice)
  "Determine whether DICE contains a 4, denoting a crew."
  (member +crew+ dice))

;; Ordered membership tests
(defun try-getting-ship ()
  "Determine whether there is a ship in *DICE*."
  (when (have-ship-p *dice*)
    (setf *ship-p* t)
    (format t "~%You found a ship!")
    (setf *dice* (bluewolf-utilities:drop +ship+ *dice*))
    (decf *dice-count*)))

(defun try-getting-captain ()
  "Determine whether there is a captain in *DICE*, pre-supposing a ship."
  (when (and *ship-p* (have-captain-p *dice*))
    (setf *captain-p* t)
    (format t "~%And we have a captain!")
    (setf *dice* (bluewolf-utilities:drop +captain+ *dice*))
    (decf *dice-count*)))

(defun try-getting-crew ()
  "Determine whether there is a crew in *DICE*, pre-supposing both ship and captain."
  (when (and *ship-p* *captain-p* (have-crew-p *dice*))
    (setf *crew-p* t)
    (format t "~%And a crew too!")
    (setf *dice* (bluewolf-utilities:drop +crew+ *dice*))
    (decf *dice-count*)))

(defun record-score ()
  "Sum *DICE*."
  (let* ((dice-score (apply #'+ *dice*))
	 (kept-score (if *kept-die* *kept-die* 0))
	 (score (+ dice-score kept-score)))
    (setf *score* score)))

;; Interface and main loop
(defun new-game ()
  (setf *dice* ())
  (setf *score* 0)
  (setf *tries* 3)
  (setf *dice-count* 5)
  (setf *kept-die* nil)
  (setf *ship-p* nil
	*captain-p* nil
	*crew-p* nil))
;; Test proper initial state of new game
(deftest proper-new-game-init
    (progn
      (new-game)
      (and (eql *dice* ())
	   (eql *score* 0)
	   (eql *tries* 3)
	   (eql *dice-count* 5)
	   (eql *kept-die* nil)
	   (eql *ship-p* nil)
	   (eql *captain-p* nil)
	   (eql *crew-p* nil)))
  t)


(defun keep-die (n &key (stream t))
  "Remove N from *DICE*, and hold it in *KEPT-DIE*."
  (setf *dice* (bluewolf-utilities:drop n *dice*))
  (decf *dice-count*)
  (setf *kept-die* n)
  (format stream "~%You kept: ~a" n)
  (format stream "~%You now have: ~a" *dice*)
  (format stream "~%Your score is: ~a" *score*)
  (format stream "~%You have ~a roll~p left." *tries* *tries*))
;; Test die keeping
(deftest die-keeping
    (let ((*dice* (list 6 2))
	  (*dice-count* 2)
	  (*score* 8))
      (keep-die 6 :stream nil)
      (record-score)
      (and (equal *dice* (list 2))
	   (eql *dice-count* 1)
	   (eql *kept-die* 6)
	   (eql *score* 8)))
  t)

(defun play ()
  (setf *dice* (roll))
  (if (zerop *tries*)
      (format t "~%Your turn is done.")
      (progn
	(decf *tries*)
	(format t "~%You threw:    ~a" *dice*)
	(unless *ship-p*
	  (try-getting-ship))
	(unless *captain-p*
	  (try-getting-captain))
	(unless *crew-p*
	  (try-getting-crew))
	(when (or *ship-p* *captain-p* *crew-p*)
	  (format t "~%You are left with:    ~a" *dice*))
	(when (and *ship-p* *captain-p* *crew-p*)
	  (record-score)
	  (format t "~%Your score is:    ~a" *score*))
	(format t "~%You have ~a roll~p left." *tries* *tries*))))
