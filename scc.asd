(defsystem :scc
  :default-component-class cl-source-file.cl
  :description "Play a simple game of Ship, Captain, and Crew."
  :version "0.0"
  :author "Udyant Wig <udyant.wig@gmail.com>"
  :licence "Undecided"
  :depends-on (:rt :bluewolf-utilities :dice)
  :components ((:file "scc")))
